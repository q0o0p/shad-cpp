#include <benchmark/benchmark.hpp>
#include <util.h>
#include <vector>
#include <stdexcept>

#include <rw_counter.h>

constexpr int kNumWriterThreads = 15;
constexpr int kNumReaderThreads = 1;

ReadWriteAtomicCounter counter;

void Basic(benchmark::State& state) {
    if (state.thread_index < kNumReaderThreads) {
        while (state.KeepRunning()) {
            counter.GetValue();
        }
    } else {
        auto incrementer = counter.GetIncrementer();
        while (state.KeepRunning()) {
            incrementer->Increment();
        }
    }
}

BENCHMARK(Basic)
    ->UseRealTime()
    ->Unit(benchmark::kNanosecond)
    ->Threads(kNumReaderThreads + kNumWriterThreads);

BENCHMARK_MAIN();
